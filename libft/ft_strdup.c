/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 16:10:57 by vbudnik           #+#    #+#             */
/*   Updated: 2017/11/11 14:31:52 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	char	*res;
	size_t	size;
	size_t	i;

	i = 0;
	size = 0;
	while (s1[size])
		size++;
	res = (char *)malloc(sizeof(*res) * (size) + 1);
	if (res == NULL)
		return (NULL);
	while (i < size)
	{
		res[i] = s1[i];
		i++;
	}
	res[i] = '\0';
	return ((char *)res);
}
