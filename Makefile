#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/04 18:32:00 by vbudnik           #+#    #+#              #
#    Updated: 2018/03/26 21:03:18 by vbudnik          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME		=			ft_ls

CC			=			gcc
FLAGS		=			-Wall -Wextra -Werror -O1 -O2 -O3 -Os -g -std=c99
SRCS		=			srcs/
INCLUDE		=			includes/
LIBFT		=			libft
COMPILED	=			main.o rec.o parsing.o for_l_1.o for_l_2.o for_l_3.o sort_file.o printing.o parsing_2.o tec.o tec_2.o teh.o

all: $(NAME)

$(NAME): $(COMPILED)
	@make -C $(LIBFT)
	@$(CC) $(FLAGS) -o $(NAME) -L $(LIBFT) -lft $(COMPILED)
	@echo "made" $(NAME)

$(COMPILED): %.o: $(SRCS)/%.c
	@$(CC) -c $(FLAGS) -I $(INCLUDE) -I $(LIBFT) $< -o $@

clean:
	@make -C $(LIBFT) clean
	@-/bin/rm -f $(COMPILED)
	@echo "cleaned" $(NAME)

fclean: clean
	@make -C $(LIBFT) fclean
	@-/bin/rm -f $(NAME)
	@echo "fcleaned" $(NAME)

re: fclean all

.PHONY: all clean fclean re