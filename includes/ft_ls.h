/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 21:29:51 by vbudnik           #+#    #+#             */
/*   Updated: 2018/03/28 22:44:13 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include <unistd.h>
# include <stdlib.h>
# include <sys/stat.h>
# include <dirent.h>
# include <time.h>
# include <stdio.h>
# include <grp.h>
# include <uuid/uuid.h>
# include "../libft/libft.h"
# include <sys/types.h>
# include <grp.h>
# include <pwd.h>

# define MAX_NAME 1024
# define SLAH  "/"

struct stat			sb;
struct stat			sb2;
struct dirent		*dp;
struct group		*grp;
struct passwd		*pwd;

typedef	enum
{
	false,
	true
}	t_bool;

typedef struct		s_options
{
	t_bool			l;
	t_bool			rec;
	t_bool			a;
	t_bool			r;
	t_bool			t;
	t_bool			one;
	t_bool			f;
}					t_options;

typedef struct		s_m_arg
{
	t_options		*opt;
	char			**res;
	int				i;
	int				k;
	char			***all_mass;
}					t_m_arg;

typedef struct		s_sok
{
	int				j;
	int				k;
	char			**m_e;
}					t_sok;

typedef struct		s_file
{
	char			*name;
	int				count_dir;
	int				count_file;
}					t_file;

struct s_sok		*g;

int					ft_dot(char *str);
void				ft_final_param(char *name, char *name_dir);
void				ft_ls_l_total(char **path);
void				ft_get_name(char *name);
void				ft_get_year(char *name);
void				ft_get_size(char *name);
void				ft_get_data(char *name);
void				ft_final_time(char *str);
int					ft_is_dir(char *name);
void				ft_para_struct_if(char av, t_options *option);
int					ft_is_dir_or_file(char *av);
void				ft_incorect_param_mess_char(char av);
void				ft_option_create(t_options **option);
void				ft_incorect_param_str(char *name);
void				ft_printf_file_type(char *name);
void				ft_read_link(char *name, char *name_dir);
void				ft_printf_2d_arr(char **mass, char *name_dir,
									t_options *option, int l);
void				ft_str_re_cut(char *s1, char *s2);
void				ft_skle(char *dir_name, char *dp, char *tmp);
int					ft_rec_ls(char *name_dir, char ***mass, int i,
							t_options *option);
int					ft_rec_counting(char **mass, char ***rec, int i,
								t_options *option);
int					ft_set_flag(char *s, t_options *opt);
void				ft_mergesort(void **tab, int length, int (*cmp)());
void				ft_fin_parth(char **av, int start, int len, char **res);
void				ft_free(char **res, t_options *opt, char ***all_mass);
void				ft_short_mess(char *v1, char *v2);
void				ft_sokrati(char ***all_mass, char **res, int i,
							t_options *opt);
int					ft_for_r_sort(const char *a, const char *b);
int					ft_for_t_sort(const char *a, const char *b);
int					ft_for_st_sort(const char *a, const char *b);
void				ft_only_first_path(char *name_dir, int i);
int					ft_sort_for_input(const char *a, const char *b);
int					ft_len_2d_array(char **arr);
void				ft_mass(char **mass_error, int len);
int					ft_cou_p(char **av);
int					ft_free_mass(int l, char **mass, char *name_dir,
								t_options *opt);
char				*ft_pat(char *res, char *av, int len);
void				ft_print_link(char *s1, char *s2);
void				ft_tech_for_sokrat(char *res);
void				ft_print_one(char *s1, char *s2);
void				ft_free_not(char **del, char ***mass);
void				ft_fin_rec(char ***mass, DIR *dir);
void				ft_fot_mass_error(char **m_e, int len);

#endif
