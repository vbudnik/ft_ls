/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   for_l_2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 20:36:41 by vbudnik           #+#    #+#             */
/*   Updated: 2018/03/23 14:18:52 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include "../libft/libft.h"

void	ft_get_name(char *name)
{
	stat(name, &sb);
	if ((pwd = getpwuid(sb.st_uid)) != NULL)
		ft_putstr(pwd->pw_name);
}

void	ft_get_year(char *name)
{
	stat(name, &sb);
	if ((grp = getgrgid(sb.st_gid)) != NULL)
		ft_putstr(grp->gr_name);
}

void	ft_get_size(char *name)
{
	stat(name, &sb);
	ft_putnbr(sb.st_size);
}

void	ft_get_data(char *name)
{
	char	*data;
	int		i;
	int		len;

	i = 0;
	len = 0;
	stat(name, &sb);
	data = ctime(&sb.st_ctime);
	len = ft_strlen(data);
	while (data[len] != ' ')
		len--;
	while (data[i] != ' ')
		i++;
	len = len - 3;
	while (i < len)
	{
		ft_putchar(data[i]);
		i++;
	}
}

void	ft_final_time(char *str)
{
	char	*a;
	int		i;
	char	**lol;

	i = 0;
	a = str;
	lol = ft_strsplit(a, ':');
	ft_putstr(*&lol[0]);
	ft_putchar(':');
	ft_putstr(*&lol[1]);
	while (lol[i] != NULL)
	{
		ft_strdel(&lol[i]);
		i++;
	}
}
