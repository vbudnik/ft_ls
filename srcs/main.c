/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 20:47:47 by vbudnik           #+#    #+#             */
/*   Updated: 2018/03/28 22:43:06 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include "../libft/libft.h"

int		main(int ac, char **av)
{
	t_m_arg		*x;

	x = (t_m_arg *)malloc(sizeof(t_m_arg));
	x->i = 0;
	x->k = 1;
	x->opt = (t_options *)malloc(sizeof(t_options));
	ft_option_create(&(x->opt));
	while (x->k < ac && ft_set_flag(av[x->k], x->opt))
		x->k = x->k + 1;
	if (ac == 1)
		x->i = ft_rec_ls(".", x->all_mass, x->i, x->opt);
	else if (x->k == ac)
		x->i = ft_rec_ls(".", x->all_mass, x->i, x->opt);
	else
	{
		if ((ac - ft_cou_p(av)) == 0)
			ft_fin_parth(av, x->k, ac, NULL);
		else
		{
			x->res = (char **)ft_memalloc(sizeof(char *) * (ft_cou_p(av)));
			ft_fin_parth(av, x->k, ac, x->res);
			ft_sokrati(x->all_mass, x->res, x->i, x->opt);
		}
	}
	return (0);
}

int		ft_dot(char *str)
{
	if (ft_strcmp(str, ".") == 0 || ft_strcmp(str, "..") == 0 || str[0] == '.')
		return (1);
	return (0);
}

int		ft_is_dir(char *name)
{
	lstat(name, &sb);
	if (S_ISDIR(sb.st_mode))
		return (1);
	return (0);
}

int		ft_is_dir_or_file(char *av)
{
	if (lstat(av, &sb) == 0)
		return (1);
	return (0);
}

int		ft_cou_p(char **av)
{
	int		i;
	int		j;

	i = 1;
	j = 0;
	while (av[i] != NULL)
	{
		if (ft_is_dir_or_file(av[i]) == 1)
			j++;
		i++;
	}
	return (j);
}
