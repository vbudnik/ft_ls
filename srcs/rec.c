/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rec.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 20:44:53 by vbudnik           #+#    #+#             */
/*   Updated: 2018/03/28 22:04:57 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include "../libft/libft.h"

void			ft_read_dir(DIR *dir, char **mass, char *dir_name,
							t_options *option)
{
	char		tmp[MAX_NAME];
	char		*del;

	while ((dp = readdir(dir)) != NULL)
	{
		if (ft_dot(dp->d_name) == 1 && option->a == false)
			continue ;
		if (ft_strcmp(dp->d_name, ".") == 0 || ft_strcmp(dp->d_name, "..") == 0)
			*mass = ft_strdup(dp->d_name);
		else
		{
			ft_strcpy(tmp, dir_name);
			ft_strcat(tmp, SLAH);
			ft_strcat(tmp, dp->d_name);
			del = *mass;
			*mass = ft_strjoin(*mass, tmp);
			ft_strdel(&del);
		}
		ft_bzero(tmp, ft_strlen(tmp));
		mass++;
	}
	*mass = NULL;
}

static int		ft_count_all(char *name_dir, t_options *opt)
{
	DIR		*dir;
	int		i;

	i = 0;
	if (!(dir = opendir(name_dir)))
		return (0);
	while ((dp = readdir(dir)) != NULL)
	{
		if (ft_dot(dp->d_name) == 1 && opt->a == false)
			continue ;
		i++;
	}
	closedir(dir);
	return (i);
}

int				ft_rec_ls(char *name_dir, char ***mass, int i,
						t_options *option)
{
	DIR		*dir;
	int		l;
	char	**del;

	l = ft_count_all(name_dir, option);
	mass = (char ***)malloc(sizeof(char **) * (i + 1));
	mass[i] = (char **)ft_memalloc(sizeof(char *) * (l + 1));
	del = mass[i];
	if (!(dir = opendir(name_dir)))
	{
		ft_free_not(del, mass);
		return (0);
	}
	ft_read_dir(dir, mass[i], name_dir, option);
	ft_only_first_path(name_dir, i);
	ft_printf_2d_arr(mass[i], name_dir, option, l);
	if (option->rec == true)
	{
		i = ft_rec_counting(mass[i], mass, i, option);
		free(del);
	}
	else
		ft_free_mass(l, mass[i], name_dir, option);
	ft_fin_rec(mass, dir);
	return (i);
}

int				ft_rec_counting(char **mass, char ***rec, int i,
								t_options *option)
{
	while (*mass)
	{
		lstat(*mass, &sb);
		if ((ft_strcmp(*mass, ".") && ft_strcmp(*mass, ".."))
			&& S_ISDIR(sb.st_mode))
		{
			i++;
			i = ft_rec_ls(*mass, rec, i, option);
			ft_strdel(mass);
		}
		ft_strdel(mass);
		mass++;
	}
	return (i);
}

void			ft_fin_rec(char ***mass, DIR *dir)
{
	free(mass);
	closedir(dir);
}
