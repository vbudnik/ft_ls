/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   for_l_1.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 20:34:22 by vbudnik           #+#    #+#             */
/*   Updated: 2018/03/26 21:19:21 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include "../libft/libft.h"

void	ft_printf_param(char *name)
{
	lstat(name, &sb);
	ft_printf_file_type(name);
	ft_putchar((sb.st_mode & S_IRUSR) ? 'r' : '-');
	ft_putchar((sb.st_mode & S_IWUSR) ? 'w' : '-');
	ft_putchar((sb.st_mode & S_IXUSR) ? 'x' : '-');
	ft_putchar((sb.st_mode & S_IRGRP) ? 'r' : '-');
	ft_putchar((sb.st_mode & S_IWGRP) ? 'w' : '-');
	ft_putchar((sb.st_mode & S_IXGRP) ? 'x' : '-');
	ft_putchar((sb.st_mode & S_IROTH) ? 'r' : '-');
	ft_putchar((sb.st_mode & S_IWOTH) ? 'w' : '-');
	ft_putchar((sb.st_mode & S_IXOTH) ? 'x' : '-');
}

void	ft_printf_file_type(char *name)
{
	lstat(name, &sb);
	if (S_ISDIR(sb.st_mode))
		ft_putchar('d');
	else if (S_ISLNK(sb.st_mode))
		ft_putchar('l');
	else if (S_ISCHR(sb.st_mode))
		ft_putchar('c');
	else if (S_ISBLK(sb.st_mode))
		ft_putchar('b');
	else if (S_ISSOCK(sb.st_mode))
		ft_putchar('s');
	else if (S_ISFIFO(sb.st_mode))
		ft_putchar('f');
	else
		ft_putchar('-');
}

void	ft_num_of_link(char *name)
{
	stat(name, &sb);
	ft_putnbr(sb.st_nlink);
}

void	ft_final_param(char *name, char *name_dir)
{
	struct stat		sb_for_link;

	lstat(name, &sb_for_link);
	ft_printf_param(name);
	ft_putstr("  ");
	ft_num_of_link(name);
	ft_putstr(" ");
	ft_get_name(name);
	ft_putstr("  ");
	ft_get_year(name);
	ft_putstr("  ");
	ft_get_size(name);
	ft_putstr(" ");
	ft_get_data(name);
	ft_putstr(" ");
	if (S_ISLNK(sb_for_link.st_mode))
		ft_read_link(name, name_dir);
	else
		ft_str_re_cut(name_dir, name);
}

void	ft_read_link(char *name, char *name_dir)
{
	ssize_t		r;
	char		*linkname;

	lstat(name, &sb);
	linkname = (char *)malloc(sizeof(char) * sb.st_size);
	if (linkname == NULL)
		return ;
	r = readlink(name, linkname, sb.st_size + 1);
	if (r >= 1024)
	{
		ft_putstr("Link filename too long\n");
		exit(0);
	}
	ft_print_link(name_dir, name);
	ft_putstr(" -> ");
	ft_putstr(linkname);
	ft_strdel(&linkname);
	free(linkname);
	write(1, "\n", 1);
}
