/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 20:40:42 by vbudnik           #+#    #+#             */
/*   Updated: 2018/03/23 16:13:31 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include "../libft/libft.h"

void	ft_para_struct_if(char av, t_options *option)
{
	if (av == 'l')
		option->l = true;
	else if (av == 'R')
		option->rec = true;
	else if (av == 'a')
		option->a = true;
	else if (av == 'r')
		option->r = true;
	else if (av == 't')
		option->t = true;
	else if (av == '1')
		option->one = true;
	else if (av == 'f')
		option->f = true;
	else
	{
		ft_incorect_param_mess_char(av);
	}
}

void	ft_incorect_param_mess_char(char av)
{
	ft_putstr("ft_ls: illegal option -- ");
	ft_putchar(av);
	ft_putstr("\n");
	ft_putstr("usage: ls [-alrtR1] [file ...]");
	ft_putstr("\n");
	exit(1);
}

void	ft_incorect_param_str(char *name)
{
	ft_putstr("ls: ");
	ft_putstr(name);
	ft_putstr(": No such file or directory");
	ft_putstr("\n");
}

int		ft_pars_name_of_dir(char *av, t_file *name)
{
	if (ft_is_dir_or_file(av) == 1)
	{
		name->name = av;
		return (1);
	}
	return (0);
}

void	ft_free(char **res, t_options *option, char ***all_mass)
{
	res = res + 0;
	if (option->l == false)
		ft_putchar('\n');
	if (option->l == true)
		ft_putchar('\r');
	if (all_mass)
		free(all_mass);
	if (option)
		free(option);
	ft_putchar('\r');
}
