/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/07 17:37:33 by vbudnik           #+#    #+#             */
/*   Updated: 2018/03/28 22:18:44 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include "../libft/libft.h"

int		ft_set_flag(char *s, t_options *opt)
{
	if (s[0] != '-')
		return (0);
	while (*(++s))
	{
		ft_para_struct_if(*s, opt);
	}
	return (1);
}

void	ft_fin_parth(char **av, int s, int len, char **res)
{
	t_sok	*g;

	g = ft_memalloc(sizeof(t_sok));
	g->j = 0;
	g->k = 0;
	if (!(ft_cou_p(av) == len))
		g->m_e = (char **)malloc(sizeof(char *) * ((len - 1) - ft_cou_p(av)));
	while (s < len)
	{
		if (ft_is_dir_or_file(av[s]) == 1)
			res[g->j++] = ft_strdup(av[s]);
		else
			g->m_e[g->k++] = ft_strdup(av[s]);
		s++;
	}
	if (res)
		res[g->j] = NULL;
	if (g->k || g->m_e)
	{
		ft_fot_mass_error(g->m_e, g->k);
	}
	free(g);
}

void	ft_option_create(t_options **option)
{
	(*option)->l = false;
	(*option)->rec = false;
	(*option)->a = false;
	(*option)->r = false;
	(*option)->t = false;
	(*option)->one = false;
}

void	ft_short_mess(char *v1, char *v2)
{
	ft_incorect_param_str(v1);
	if (ft_is_dir_or_file(v2))
		ft_putchar('\n');
}

void	ft_sokrati(char ***all_mass, char **res, int i, t_options *opt)
{
	int		k;
	int		len;

	k = 0;
	len = ft_len_2d_array(res);
	ft_mergesort((void **)res, len, ft_sort_for_input);
	while (k < len)
	{
		lstat(res[k], &sb);
		if (ft_is_dir(res[k]) == 1 && k > 0)
		{
			ft_putchar('\n');
			ft_putendl(res[k]);
		}
		if (S_ISREG(sb.st_mode))
			ft_tech_for_sokrat(res[k++]);
		else
		{
			i = ft_rec_ls(res[k], all_mass, i, opt);
			ft_strdel(&res[k]);
			ft_putchar('\n');
			k++;
		}
	}
	free(res);
}
