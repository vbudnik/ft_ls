/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tec_2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/11 20:20:40 by vbudnik           #+#    #+#             */
/*   Updated: 2018/03/28 19:43:16 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include "../libft/libft.h"

int		ft_len_2d_array(char **arr)
{
	int		i;

	i = 0;
	while (arr[i])
		i++;
	return (i);
}

void	ft_mass(char **mass_error, int len)
{
	int		i;

	i = 0;
	ft_mergesort((void **)mass_error, len, ft_for_st_sort);
	while (i < len)
	{
		ft_incorect_param_str(mass_error[i]);
		ft_strdel(&mass_error[i]);
		i++;
	}
	free(mass_error);
}
